Ext.define('Pertemuan9.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',

    requires: [
        'Ext.MessageBox',

        'Pertemuan9.view.main.MainController',
        'Pertemuan9.view.main.MainModel',
        'Pertemuan9.view.main.List',
        'Pertemuan9.view.dataview.BasicDataView',
        'Pertemuan9.view.main.FormPanel',
        'Pertemuan9.view.main.Bar',
        'Pertemuan9.store.Bar',
        'Pertemuan9.view.main.ListBar'
    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        },
        styleHtmlContent: true
    },

    tabBarPosition: 'bottom',

    items: [ 
       {
            docked: 'top',
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Read',
                    ui: 'action',
                    scope: this,
                    listeners: {
                        tap: 'onReadClicked'
                    }
                }
            ]
        },
        {
            title: 'Home',
            iconCls: 'x-fa fa-home',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype:'panel',
                layout: 'hbox',
                items:[{
                xtype: 'mainlist',
                flex:2
                },{
                   xtype :'detail', 
                   flex:1
                },{
                    xtype :'editform', 
                    flex:1
                 }
            ]
            }]
        },{
            title: 'Chart',
            iconCls: 'x-fa fa-user',
            layout: 'fit',
            items: [{
                xtype:'panel',
                layout: 'vbox',
                items:[{
                xtype: 'bar',
                flex:1
                },{
                   xtype :'listbar', 
                   flex:1
                }
            ]
            }]
        },{
            title: 'Groups',
            iconCls: 'x-fa fa-users',
            bind: {
                html: '{loremIpsum}'
            }
        },{
            title: 'Settings',
            iconCls: 'x-fa fa-cog',
            bind: {
                html: '{loremIpsum}'
            }
        }
    ]
});